;;;; -*- Mode: Lisp -*-

;;;; ook.asd

(asdf:defsystem :ook
  :description "A CL compiler and enviroment for literate Orangutans."
  :version "00K"
  :author "Marco 'Wizzard' Antoniotti"
  :licence "Public Domain"
  :components ((:file "ook-package")
               (:file "ook"
                :depends-on ("ook-package")))
  )

;;;; end of file -- ook.asd

